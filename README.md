# Z5 Project Training

### Objective
	Create a sign and login module as part of the project training.

### Modules
    1. Welcome page
    2. Sign up page
    3. Login page
    4. Home page

### Technologies Used
	HTML, CSS, JS, Bootstrap

### Features
    1. Welcome page (index.html)
        - An entry page with sign up and login in buttons with basic information regarding the website
    2. Sign up page
        - This page will have a form with 7 fields
            - Name (text)
            - Email id (email)
            - Password (password); It should contain 1 uppercase, 1 lowercase & 1 number (regular expressions)
            - Age (int)
            - Skillset (checkboxes)
            - Gender (radio)
            - Country, State (dropdown, api call)
            - Date of registration (date)
        - A new user will be created in the local storage when the form is submitted.
        - On form submission, user will be redirected to the Home page
    3. Login page
        - This page will have a form with two fields- email id and password.
        - Form Validation will be done to check if the email already exists & if the email and password matches. 
        - If it doesn’t match the appropriate message is displayed.
        - If it matches, then the user will be redirected to the Home page.
    4. Home page
        - Users can view their personal details.
        - Users can edit their personal details. 
        - Users can edit their password.
        - Users can view the employee directory.
        - The session gets timed out after 1 min of inactivity.
        - The user can logout from the session.
    5. Other Features
        - Responsive on mobiles and tablets.

### Theme
    - Minimalistic
    - Primary Color: Black & White
    - Accent Color: #00305e, #a2222c
    - Font: Helvetica sans serif
