//To offset main content when offcanvas appears
let sidebar_toggle= document.querySelector(".bi-list");
let maincontent= document.querySelector(".main-content");
let offCanvas= document.querySelector(".offcanvas");
sidebar_toggle.addEventListener("click", () =>{
    maincontent.classList.add("col-lg-10");
    maincontent.classList.add("offset-lg-2");
  });

//To remove offset when offcanvas disppears
let sidebar_close= document.querySelector(".btn-close");
sidebar_close.addEventListener("click", () =>{
    maincontent.classList.remove("col-lg-10");
    maincontent.classList.remove("offset-lg-2");
  });

//To load offcanvas automatically only when screen size is above 1300px
function offcanvasLoad(x) {
    if (x.matches) { 
        maincontent.classList.remove("col-lg-10");
        maincontent.classList.remove("offset-lg-2");
        offCanvas.classList.remove("show");
    }
  }
  
let x = window.matchMedia("(max-width: 1300px)")
offcanvasLoad(x);


// session timeout function
(function() {

  const redirectUrl = 'login.html';
  
  let idleTimeout;

  const resetIdleTimeout = function() {

      // Clears the existing timeout
      if(idleTimeout) {
        clearTimeout(idleTimeout);
      }

      // redirects to the new page after given number of seconds
      idleTimeout = setTimeout(function() {
        alert("Session timed out")
        location.href = redirectUrl
        sessionStorage.clear();
      }, 60000);
  };

  // Init on page load and also resets the timer
  resetIdleTimeout();

  // Reset the timeout timer in case of any activity
  ['click', 'touchstart', 'mousemove'].forEach(evt => 
      document.addEventListener(evt, resetIdleTimeout, false)
  );

})();

//logout function
function logout(){
  sessionStorage.clear();
}

// fill in user details
let currentUserKey= sessionStorage.key(0);
let currentUser= JSON.parse(localStorage.getItem(currentUserKey));
document.querySelector(".name").innerHTML= currentUser.name;
document.querySelector(".emailid span").innerHTML= currentUserKey;
document.querySelector(".age span").innerHTML= currentUser.age;
document.querySelector(".skillset span").innerHTML= currentUser.skills;
document.querySelector(".gender span").innerHTML= currentUser.gender;
document.querySelector(".country span").innerHTML= currentUser.country;
document.querySelector(".state span").innerHTML= currentUser.state;
document.querySelector(".dor span").innerHTML= currentUser.date;


// employee directory table section

const tableData = []

// loop which stores data from local storage into the tableData array
for (var i = 0; i < localStorage.length; i++){
  tableData.push(JSON.parse(localStorage.getItem(localStorage.key(i))));
  delete tableData[i]['date']
  delete tableData[i]['password']
}


// generating a table from the data from local storage

function generateTableHead(table, data) {
  let thead = table.createTHead();
  let row = thead.insertRow();
  for (let key of data) { // iterating through the column headers -> name, age, etc.
    let th = document.createElement("th");
    let text = document.createTextNode(key);
    th.appendChild(text);
    row.appendChild(th);
  }
}

function generateTable(table, data) {
  for (let element of data) {
    let row = table.insertRow(); // insertRow() is used instead of createElement()
    for (key in element) {
      let cell = row.insertCell(); // creates a <td> element
      let text = document.createTextNode(element[key]);
      cell.appendChild(text);
    }
  }
}

let table = document.querySelector("table");
let data = Object.keys(tableData[0]); // data contains column headers -> name, gender, skills etc
generateTable(table, tableData);
generateTableHead(table, data);