// global variables that check if password contains the required characters
let lower_check = 0
let upper_check = 0
let number_check = 0
let symbol_check = 0

// function to hide and show password
function showHide() {
    const temp = document.querySelector('#pass')
        if (temp.type === "password") {
            temp.type = "text";
        }
        else {
            temp.type = "password";
        }
}

// function to show password criteria
function displayPass() {
    const showPass = document.querySelector('.pass-check')
    const eye = document.getElementById('eye')

    showPass.style.display = 'block'
    eye.style.top = "31%"
}

// function to hide password criteria
function hidePass() {
    const showPass = document.querySelector('.pass-check')
    const eye = document.getElementById('eye')

    showPass.style.display = 'none'
    eye.style.top = "33%"
}


function passCheck(data) {

    const pass_checks = document.querySelectorAll('.password-checker')

    // regular expressions for the different conditions
    const lower = new RegExp('(?=.*[a-z])')
    const upper = new RegExp('(?=.*[A-Z])')
    const number = new RegExp('(?=.*[0-9])')
    const special = new RegExp('(?=.*[!@#\$%&\*])')

    // performing the checks
    if(lower.test(data)) {
        pass_checks[0].style.color = 'green'
        lower_check = 1
    } else {
        pass_checks[0].style.color = '#6c757d'
        lower_check = 0
    }

    if(upper.test(data)) {
        pass_checks[1].style.color = 'green'
        upper_check = 1
    } else {
        pass_checks[1].style.color = '#6c757d'
        upper_check = 0
    }

    if(number.test(data)) {
        pass_checks[2].style.color = 'green'
        number_check = 1
    } else {
        pass_checks[2].style.color = '#6c757d'
        number_check = 0
    }

    if(special.test(data)) {
        pass_checks[3].style.color = 'green'
        symbol_check = 1
    } else {
        pass_checks[3].style.color = '#6c757d'
        symbol_check = 0
    }
}


// autofill details from local storage into form
let currentUserKey= sessionStorage.key(0);
let currentUser= JSON.parse(localStorage.getItem(currentUserKey));
document.getElementById("name").value= currentUser.name;
document.getElementById("email").value= currentUserKey;
document.getElementById("age").value= currentUser.age;
document.getElementById("pass").value= currentUser.password;
document.getElementById("country").value= currentUser.country;
document.getElementById("state").value= currentUser.state;


const checks = document.querySelectorAll('.checkBox');
for(let i = 0; i < checks.length; i++) {
    for(let j=0; j < currentUser.skills.length; j++) {
        if(checks[i].value == currentUser.skills[j]) {
            checks[i].checked = true;
        }
    } 
}


const genders = document.getElementsByName('gender');
for(let i =0; i < genders.length; i++) {
    if(genders[i].value == currentUser.gender) {
        genders[i].checked = true;
    }
}

// updating values
const updateForm = document.getElementById('updateForm');

updateForm.addEventListener('submit', (e)=>{

    // checking if password is strong
    if(number_check && symbol_check && lower_check && upper_check === 1) {

        const password= document.querySelector("#pass");
        const skillsDiv= document.querySelectorAll('input[type=checkbox]:checked');
        
        let skills= [];
        for(var i = 0; i < skillsDiv.length; i++){
            if(skillsDiv[i].checked){
                skills.push(skillsDiv[i].value);
            }
        }

    let userDetails= JSON.parse(localStorage.getItem(currentUserKey));
    userDetails['password']= password.value;
    userDetails['skills']= skills;
    localStorage.setItem(currentUserKey, JSON.stringify(userDetails));
    
    } else {
        alert("Password not strong enough");
        e.preventDefault();
    }

});