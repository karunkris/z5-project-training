const loginForm = document.getElementById('loginForm');
loginForm.addEventListener('submit', (e)=>{
    const email= document.querySelector("#email");
    const password= document.querySelector("#pass");
    let userExists = false;
    let correctPassword = false;

    for(var i =0; i< localStorage.length; i++){
        let emailKey= localStorage.key(i);
        let user = JSON.parse(localStorage.getItem(emailKey));
        if(localStorage.key(i) === email.value){
            userExists = true;
            if(user.password === password.value){
            //Creating a session storage of the user
            sessionStorage.setItem(email.value, password.value);
            //alert(localStorage.getItem(email.value));
            correctPassword = true;
            }
        }
    }

    if(!userExists){
        alert("User doesn't exist");
        e.preventDefault();
    }else if(!correctPassword){
        alert("Incorrect Password!");
        e.preventDefault();
    }
    
});