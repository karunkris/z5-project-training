// global variables that check if password contains the required characters
let lower_check = 0
let upper_check = 0
let number_check = 0
let symbol_check = 0


// function to hide and show password
function showHide() {
    const temp = document.querySelector('#pass')
        if (temp.type === "password") {
            temp.type = "text"
        }
        else {
            temp.type = "password"
        }
}

// function to show password criteria
function displayPass() {
    const showPass = document.querySelector('.pass-check')
    const eye = document.getElementById('eye')

    showPass.style.display = 'block'
    eye.style.top = "25%"
}

// function to hide password criteria
function hidePass() {
    const showPass = document.querySelector('.pass-check')
    const eye = document.getElementById('eye')

    showPass.style.display = 'none'
    eye.style.top = "27%"
}


function passCheck(data) {

    const pass_checks = document.querySelectorAll('.password-checker')

    // regular expressions for the different conditions
    const lower = new RegExp('(?=.*[a-z])')
    const upper = new RegExp('(?=.*[A-Z])')
    const number = new RegExp('(?=.*[0-9])')
    const special = new RegExp('(?=.*[!@#\$%&\*])')

    // performing the checks
    if(lower.test(data)) {
        pass_checks[0].style.color = 'green'
        lower_check = 1
    } else {
        pass_checks[0].style.color = '#6c757d'
        lower_check = 0
    }

    if(upper.test(data)) {
        pass_checks[1].style.color = 'green'
        upper_check = 1
    } else {
        pass_checks[1].style.color = '#6c757d'
        upper_check = 0
    }

    if(number.test(data)) {
        pass_checks[2].style.color = 'green'
        number_check = 1
    } else {
        pass_checks[2].style.color = '#6c757d'
        number_check = 0
    }

    if(special.test(data)) {
        pass_checks[3].style.color = 'green'
        symbol_check = 1
    } else {
        pass_checks[3].style.color = '#6c757d'
        symbol_check = 0
    }
}

const signUpForm = document.getElementById('signUpForm');

signUpForm.addEventListener('submit', (e)=> {

    // checking if password contains required characters
    if(number_check && symbol_check && lower_check && upper_check === 1) {

        // current date using 'luxon' library
        const date = luxon.DateTime.now().toLocaleString(luxon.DateTime.DATETIME_MED)
        
        const name= document.querySelector("#name");
        const email= document.querySelector("#email");
        const password= document.querySelector("#pass");
        const age= document.querySelector("#age");

        const countrySelect= document.querySelector("#country");
        const country = countrySelect.options[countrySelect.selectedIndex];
        const stateSelect= document.querySelector("#state");

        const state = stateSelect.options[stateSelect.selectedIndex];
        const gender= document.querySelector('input[type=radio]:checked');
        const skillsDiv= document.querySelectorAll('input[type=checkbox]:checked');
        
        let skills= [];
        for(var i = 0; i < skillsDiv.length; i++){
            if(skillsDiv[i].checked){
                skills.push(skillsDiv[i].value);
            }
        }

        if(localStorage.getItem(email.value) === null){
            
            //Updating a users details into the local storage
            localStorage.setItem(email.value, JSON.stringify({name: name.value, password: password.value, age: age.value, country: country.value, state: state.value, gender: gender.value, skills: skills, date: date}))
            
            //Creating a session storage of the user
            sessionStorage.setItem(email.value, password.value);
            
            //alert(localStorage.getItem(email.value));
            
        } else{
            alert("User already exists!");
            e.preventDefault();
        }
    
    } else {
        alert("Password not strong enough")
        e.preventDefault()
    }
});